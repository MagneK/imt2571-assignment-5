<?php

include_once("Skier.php");
include_once("Club.php");
include_once("LogEntry.php");
include_once("SeasonParticipant.php");
include_once("DBProps.php");

/** The DBModel is the class responsible of inserting the data to the database.
 * 	Code snippets taken from database assignment 1
 */
class DBModel
{
	/**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try
			{
				$this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PWD);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e)
			{
				error_log($e->getMessage());
				echo "Failed to connect to the database.";
			}
		}
    }
	
	/** Function that checks if you have successfully connected to a database
	 * @return TRUE|FALSE False if $db is null; True otherwise.
     */
	public function isConnected()
	{
		if ($this->db)
			return true;
		return false;
	}
	
	/** Function adding an array of skier objects to the sql database.
	 * @param Skier[] An array of skier objects to be added.
     */
	public function addSkiers($skiers)
	{
		try
		{	
			$stmt = $this->db->prepare('INSERT INTO skier(userName, firstName, lastName, yearOfBirth) VALUES(:un, :fn, :ln, :yob)');
			$count = 0;
			
			foreach ($skiers as $skier)
			{
				$stmt->bindValue(':un', $skier->userName);
				$stmt->bindValue(':fn', $skier->firstName);
				$stmt->bindValue(':ln', $skier->lastName);
				$stmt->bindValue(':yob', $skier->yearOfBirth);
				$stmt->execute();
				$count++;
			}
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			echo $e->getMessage() . "<br>";
			echo "Failed inserting all skiers. <br>";
		}
		echo "inserted " . $count . " skiers.<br>";
	}
	
	/** Function adding an array of club objects to the sql database.
	 * @param Club[] An array of club objects to be added.
     */
	public function addClubs($clubs)
	{
		try
		{	
			$stmt = $this->db->prepare('INSERT INTO club(id, name, city, county) VALUES(:id, :na, :ci, :co)');
			$count = 0;
			
			foreach ($clubs as $club)
			{
				$stmt->bindValue(':id', $club->id);
				$stmt->bindValue(':na', $club->name);
				$stmt->bindValue(':ci', $club->city);
				$stmt->bindValue(':co', $club->county);
				$stmt->execute();
				$count++;
			}
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			echo $e->getMessage() . "<br>";
			echo "Failed inserting all clubs. <br>";
		}
		echo "inserted " . $count . " clubs.<br>";
	}
	
	/** Function adding an array of logEntry objects to the sql database.
	 * @param LogEntry[] An array of logEntry objects to be added.
     */
	public function addLogEntries($logEntries)
	{
		try
		{	
			$stmt = $this->db->prepare('INSERT INTO log_entry(skierUserName, _date, area, distance) VALUES(:un, :da, :ar, :di)');
			$count = 0;
			
			foreach ($logEntries as $logEntry)
			{
				$stmt->bindValue(':un', $logEntry->skierUserName);
				$stmt->bindValue(':da', $logEntry->logDate);
				$stmt->bindValue(':ar', $logEntry->area);
				$stmt->bindValue(':di', $logEntry->distance);
				$stmt->execute();
				$count++;
			}
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			echo $e->getMessage() . "<br>";
			echo "Failed inserting all log entries. <br>";
		}
		echo "inserted " . $count . " log entries.<br>";
	}
	
	/** Function adding an array of seasonParticipant objects to the sql database.
	 * @param SeasonParticipant[] An array of seasonParticipant objects to be added.
     */
	public function addSeasonParticipants($seasonParticipants)
	{
		try
		{	
			$stmt = $this->db->prepare('INSERT INTO season_participant(skierUserName, fallYear, totalDistance, clubId) VALUES(:un, :fy, :td, :id)');
			$count = 0;
			
			foreach ($seasonParticipants as $seasonParticipant)
			{
				$stmt->bindValue(':un', $seasonParticipant->skierUserName);
				$stmt->bindValue(':fy', $seasonParticipant->fallYear);
				$stmt->bindValue(':td', $seasonParticipant->totalDistance);
				$stmt->bindValue(':id', $seasonParticipant->clubId);
				$stmt->execute();
				$count++;
			}
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			echo $e->getMessage() . "<br>";
			echo "Failed inserting all season participants. <br>";
		}
		echo "inserted " . $count . " season participants.<br>";
	}
}

?>