<?php

class SeasonParticipant
{
	public $skierUserName;
	public $fallYear;
	public $totalDistance;
	public $clubId;
	
	public function __construct($un, $fy, $td, $id)  
    {  
		$this->skierUserName = $un;
		$this->fallYear = $fy;
		$this->totalDistance = $td;
		$this->clubId = $id;
    } 
}

?>