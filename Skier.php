<?php

class Skier
{
	public $userName;
	public $firstName;
	public $lastName;
	public $yearOfBirth;
	
	public function __construct($un, $fn, $ln, $yob)  
    {  
		$this->userName = $un;
		$this->firstName = $fn;
		$this->lastName = $ln;
		$this->yearOfBirth = $yob;
    } 
}

?>