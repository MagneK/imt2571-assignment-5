<?php

include_once("Skier.php");
include_once("Club.php");
include_once("LogEntry.php");
include_once("SeasonParticipant.php");
include_once("XMLModel.php");
include_once("DBModel.php");

$model_XML = new XMLModel();
$model_db = new DBModel();

if ($model_db->isConnected() && $model_XML->loaded)
{
	$skiers = $model_XML->getSkiers();
	$clubs = $model_XML->getClubs();
	$entries = $model_XML->getLogEntries();
	$participants = $model_XML->getSeasonParticipants();

	$model_db->addSkiers($skiers);
	$model_db->addClubs($clubs);
	$model_db->addLogEntries($entries);
	$model_db->addSeasonParticipants($participants);
}

?>