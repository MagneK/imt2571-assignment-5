<?php

include_once("Skier.php");
include_once("Club.php");
include_once("LogEntry.php");
include_once("SeasonParticipant.php");

class XMLModel
{
	private $doc;
	private $xpath;
	public $loaded;

	public function __construct()  
    { 
		$this->doc = new DOMDocument();
		@$this->loaded = $this->doc->load(XML_DOC);
		if ($this->loaded)
		{
			$this->doc->normalize();
			$this->xpath = new DOMXPath($this->doc);
		}
		else
		{
			echo "Failed to load " . XML_DOC . "<br>";
		}
	}
	
	/** Function getting relevant data from the DOM tree and converting it to an array of all skier objects.
	 * @return Skier[] An array of skier objects gotten from the DOM tree.
     */
	public function getSkiers()
	{
		$skiersNodes = $this->doc->documentElement->childNodes[3]->childNodes;
		$index = 0;
		$skiers = array();

		foreach ($skiersNodes as $skier)
		{
			if ($skier->nodeType == XML_ELEMENT_NODE)
			{
				$userName = $skier->attributes->item(0)->textContent;
				$dataIndex = 0;
				
				foreach ($skier->childNodes as $skierNode)
				{
					if ($skierNode->nodeType == XML_ELEMENT_NODE)
					{
						$data[$dataIndex++] = $skierNode->textContent;
					}
				}
				
				$skiers[$index++] = new Skier($userName, $data[0], $data[1], $data[2]);
			}
		}
		
		return $skiers;
	}
	
	/** Function getting relevant data from the DOM tree and converting it to an array of all club objects.
	 * @return Club[] An array of club objects gotten from the DOM tree.
     */
	public function getClubs()
	{
		$clubsNodes = $this->doc->documentElement->childNodes[1]->childNodes;
		$index = 0;
		$clubs = array();

		foreach ($clubsNodes as $club)
		{
			if ($club->nodeType == XML_ELEMENT_NODE)
			{
				$id = $club->attributes->item(0)->textContent;
				$dataIndex = 0;
				
				foreach ($club->childNodes as $clubNode)
				{
					if ($clubNode->nodeType == XML_ELEMENT_NODE)
					{
						$data[$dataIndex++] = $clubNode->textContent;
					}
				}
				
				$clubs[$index++] = new Club($id, $data[0], $data[1], $data[2]);
			}
		}
		
		return $clubs;
	}
	
	/** Function getting relevant data from the DOM tree and converting it to an array of all logEntry objects.
	 * @return LogEntry[] An array of logEntry objects gotten from the DOM tree.
     */
	public function getLogEntries()
	{
		$skiersNodes = $this->xpath->query('/SkierLogs/Season/Skiers/Skier');
		$index = 0;
		$entries = array();


		// Loop skier
		foreach ($skiersNodes as $skiersNode)
		{
			$userName = $skiersNode->attributes->item(0)->textContent;
							
			// Loop log
			foreach ($skiersNode->childNodes as $skierNode)
			{
				if ($skierNode->nodeType == XML_ELEMENT_NODE)
				{
					// Loop entry
					foreach ($skierNode->childNodes as $logNode)
					{
						if ($logNode->nodeType == XML_ELEMENT_NODE)
						{
							$dataIndex = 0;
											
							// Loop entry elements
							foreach ($logNode->childNodes as $entryNode)
							{
								if ($entryNode->nodeType == XML_ELEMENT_NODE)
								{
									$data[$dataIndex++] = $entryNode->textContent;
								}
							}
							
							$entries[$index++] = new LogEntry($userName, $data[0], $data[1], $data[2]);
						}
					}
				}
			}
		}
		
		return $entries;
	}
	
	/** Function getting relevant data from the DOM tree and converting it to an array of all seasonParticipant objects.
	 * @return SeasonParticipant[] An array of seasonParticipant objects gotten from the DOM tree.
     */
	public function getSeasonParticipants()
	{
		$seasons = $this->xpath->query('/SkierLogs/Season');
		$index = 0;
		$seasonParticipants = array();

		foreach ($seasons as $season)
		{
			$fallYear = $season->attributes->item(0)->textContent;
			
			foreach ($season->childNodes as $seasonNodes)
			{
				if ($seasonNodes->nodeType == XML_ELEMENT_NODE)
				{
					$clubId = null;
					if ($seasonNodes->hasAttributes())
					{
						$clubId = $seasonNodes->attributes->item(0)->textContent;
					}
					
					foreach ($seasonNodes->childNodes as $skiersNodes)
					{
						if ($skiersNodes->nodeType == XML_ELEMENT_NODE)
						{
							$userName = $skiersNodes->attributes->item(0)->textContent;
							$totalDistance = $this->xpath->evaluate('sum(/SkierLogs/Season[@fallYear = \'' . $fallYear . '\']/Skiers/Skier[@userName = \'' . $userName . '\']/Log/Entry/Distance)');
							
							$seasonParticipants[$index++] = new SeasonParticipant($userName, $fallYear, $totalDistance, $clubId);
						}
					}
				}
			}
		}
		
		return $seasonParticipants;
	}
}

?>