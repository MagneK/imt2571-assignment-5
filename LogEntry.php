<?php

class LogEntry
{
	public $skierUserName;
	public $logDate;
	public $area;
	public $distance;
	
	public function __construct($un, $da, $ar, $di)  
    {  
		$this->skierUserName = $un;
		$this->logDate = $da;
		$this->area = $ar;
		$this->distance = $di;
    } 
}

?>