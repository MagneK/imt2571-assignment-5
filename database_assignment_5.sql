-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2017 at 04:34 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_assignment_5`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` varchar(12) NOT NULL,
  `name` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `county` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_entry`
--

CREATE TABLE `log_entry` (
  `skierUserName` varchar(60) NOT NULL,
  `_date` date NOT NULL,
  `area` varchar(60) NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `season_participant`
--

CREATE TABLE `season_participant` (
  `skierUserName` varchar(60) NOT NULL,
  `fallYear` int(11) NOT NULL,
  `totalDistance` int(11) NOT NULL,
  `clubId` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `season_participant`
--
DELIMITER $$
CREATE TRIGGER `season_participant_insert_trigger` BEFORE INSERT ON `season_participant` FOR EACH ROW BEGIN
	IF NEW.fallYear < 2000 OR NEW.fallYear > 9999 THEN
    	SIGNAL SQLSTATE '45000'
      		SET MESSAGE_TEXT = 'Integrity ERROR (season_participant): fallYear must be between 2000 and 9999';
    END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `season_participant_update_trigger` BEFORE UPDATE ON `season_participant` FOR EACH ROW BEGIN
	IF NEW.fallYear < 2000 OR NEW.fallYear > 9999 THEN
    	SIGNAL SQLSTATE '45000'
      		SET MESSAGE_TEXT = 'Integrity ERROR (season_participant): fallYear must be between 2000 and 9999';
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) NOT NULL,
  `yearOfBirth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `skier`
--
DELIMITER $$
CREATE TRIGGER `skier_insert_trigger` BEFORE INSERT ON `skier` FOR EACH ROW BEGIN
	IF NEW.yearOfBirth < 1900 OR NEW.yearOfBirth > 9999 THEN
    	SIGNAL SQLSTATE '45000'
      		SET MESSAGE_TEXT = 'Integrity ERROR (skier): yearOfBirth must be between 1900 and 9999';
    END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `skier_update_trigger` BEFORE UPDATE ON `skier` FOR EACH ROW BEGIN
	IF NEW.yearOfBirth < 1900 OR NEW.yearOfBirth > 9999 THEN
    	SIGNAL SQLSTATE '45000'
      		SET MESSAGE_TEXT = 'Integrity ERROR (skier): yearOfBirth must be between 1900 and 9999';
    END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_entry`
--
ALTER TABLE `log_entry`
  ADD PRIMARY KEY (`skierUserName`,`_date`,`area`);

--
-- Indexes for table `season_participant`
--
ALTER TABLE `season_participant`
  ADD PRIMARY KEY (`skierUserName`,`fallYear`),
  ADD KEY `clubId` (`clubId`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `log_entry`
--
ALTER TABLE `log_entry`
  ADD CONSTRAINT `log_entry_ibfk_1` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`) ON UPDATE CASCADE;

--
-- Constraints for table `season_participant`
--
ALTER TABLE `season_participant`
  ADD CONSTRAINT `season_participant_ibfk_1` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`) ON UPDATE CASCADE,
  ADD CONSTRAINT `season_participant_ibfk_2` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
